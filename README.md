### 🏹

<!--
**yhm-amber/yhm-amber** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->


~~~
僭越之人呐。

你的旅途已经到达终点，
但你仍未跨越最后的门扉。

如果你明白旅途的意义，
就上前来、击败我、命令我让路。

向我证明，
你比我更适合拯救她。

然后，
再去纺织新的命运罢。
~~~


在高一点的角度去审视，这就是反思。

不管对于任何现象，即便并且特别是发生在自己身上的。

比如自己在认为「哦哟这个好对/错啊」的时候，应有审视，并思考是什么导致了这一现象的、这一现象对于各方的发展而言意义是什么，等等。

然后，你会看到一些原本看不到的。

并且，这也会为你带来行动的勇气。
